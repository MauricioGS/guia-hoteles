$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval:4000
			});
			/*Uso de eventos con jquery debe tener en nombre del modal asi: $('#contacto2') no del boton: $('#contacto2btn')*/
			$('#contacto2').on('show.bs.modal', function (e) {
				console.log("el model se está mostrando");
				$('#contacto2btn').removeClass('btn-outline-success');
				$('#contacto2btn').addClass('btn-primary');
				/*Para deshabilitar el botón*/
				$('#contacto2btn').prop("disabled",true);
			});
			$('#contacto2').on('shown.bs.modal', function (e) {
				console.log("el model se mostró");
			});
			$('#contacto2').on('hide.bs.modal', function (e) {
				console.log("el model se oculta");
			});
			$('#contacto2').on('hidden.bs.modal', function (e) {
				console.log("el model se ocultó");
				$('#contacto2btn').removeClass('btn-primary');
				$('#contacto2btn').addClass('btn-outline-success');
				/*Para activar el botón luego de haberlo deshabilitado*/
				$('#contacto2btn').prop("disabled",false);
			});
			
		});